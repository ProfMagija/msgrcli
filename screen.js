module.exports = (() => {
  const bls = require('blessed')
  let Screen = {}

  let screen = null
  let header = null
  let main = null
  let statusline = null
  let commandline = null

  Screen.refresh = function() {
    header.setContent(Screen.funcs.header())
    statusline.setContent(Screen.funcs.status())
    main.setItems(Screen.funcs.data())
    if (Screen.last) {
      main.up()
      main.down(main.items.length)
    }
    main.show()
    screen.render()
  }

  function setup_fields() { // {{{
    screen = bls.screen({
      smartCSR: true,
      dockBorders: true,
      fullUnicode: true,
      title: 'msgrcli'
    })
    header = bls.box({
      top: 0, left: 0, right: 0, height: 1,
      tags: true,
      style: {
        fg: 'white', bg: 'blue'
      }
    })
    main = bls.list({
      top: 1, left: 0, right: 0, bottom: 2,
      tags: true,
      style: {
        fg: 'white', bg: 'black',
        selected: {
          bg: '#222222', fg: null
        }
      }
    })
    statusline = bls.box({
      left: 0, right: 0, bottom: 1, height: 1,
      tags: true,
      style: {
        fg: 'white', bg: 'blue'
      }
    })
    commandline = new bls.Textbox({
      left: 0, right: 0, bottom: 0, height: 1,
      tags: true,
      style: {
        fg: 'white', bg: 'black'
      }
    })
    screen.append(header)
    screen.append(main)
    screen.append(statusline)
    screen.append(commandline)
    screen.render()
  } // }}}

  Screen.echo = function(data) {
    commandline.setContent(data || '')
    screen.render()
  }

  Screen.prompt = function(pr, cb) {
    commandline.setValue(pr)
    screen.render()
    commandline.readInput(cb)
  }

  Screen.up = (v) => { main.up(v); screen.render() }
  Screen.down = (v) => { main.down(v); screen.render() }
  Screen.getSelected = () => main.selected
  Screen.scrollTo = i => { Screen.last = false; main.up(main.items.length); if (i) main.down(i) }
  Screen.scrollToLast = () => { Screen.last = true; main.down(main.items.length) }
  Screen.unselect = b => main._isList = !b

  Screen.keymap = (a, b) => screen.key(a, b)

  Screen.init = function(getHeader, getStatus, getData) {
    Screen.funcs = {
      header: getHeader,
      status: getStatus,
      data: getData
    }
    setup_fields()
    Screen.refresh()
  }

  return Screen
})()

// vim: fdm=marker sw=2
