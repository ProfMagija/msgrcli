# msgrcli - Facebook Messenger CLI

# Running

```shell
git clone https://gitlab.com/ProfMagija/msgrcli.git
cd msgrcli
npm install
node index
```

# Found a bug? Crashed?

Submit an issue! Try to give as much info about what happened at the moment
of crash (what message you recieved, check messenger on web/phone, ...)
