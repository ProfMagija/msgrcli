let path = require('path')
let os = require('os')
let read = require('read')
let data = require('./data.js')
let screen = require('./screen.js')
let events = require('./events.js')
let format = require('./format.js')

let curThread = -1
let homePos = 0
let openThreads = []
let _threads = [];

let insertMode = null
function setMode(mode) {
  insertMode = mode
  screen.refresh()
}

//((ce) => { console.error = (data) => { ce(data); process.exit(1) } })(console.error)

function getHeader() {
  return format.header(curThread, openThreads)
}

function getStatus() {
  return format.status(curThread, openThreads, insertMode)
}

function getData() {
  if (curThread == -1) {
    _threads = data.getThreads()
    return _threads.map(format.thread)
  } else {
    return data.getThreadHistory(openThreads[curThread]).map(format.chatLine)
  }
}

function changeScreen(s) {
  if (s < -1 || s >= openThreads.length) return
  curThread = s
  if (s == -1) {
    screen.scrollTo(homePos)
  } else {
    screen.scrollToLast()
  }
  screen.refresh()
}

function openThread(tid) {
  let x = openThreads.indexOf(tid)
  if (x == -1) {
    openThreads.push(tid)
    changeScreen(openThreads.length - 1)
  } else {
    changeScreen(x)
  }
}

function homeSelect() {
  if (curThread != -1) return
  homePos = screen.getSelected()
  openThread(_threads[homePos])
}

let cmds = { 
  quit: () => process.exit(0),
  q: () => process.exit(0),
  eval: (cmd) => screen.echo((eval(cmd.substr(5)) || '').toString())
}

function doCmd(cmd) {
  let ncmd = cmd.split(' ')[0]
  if (cmds[ncmd]) cmds[ncmd](cmd)
  else screen.echo('unknown cmd :' + ncmd)
}

function messageEdit(err, val) {
  if (err) return console.error(err)
  setMode(null)
  if (!val) return
  data.sendMessage(openThreads[curThread], val, (err) => {
    if (err) return console.error(err)
  })
  setMode('INSERT')
  screen.prompt('', messageEdit)
}

function setup_keys() {
  screen.keymap('q', () => {
    if (curThread == -1)
      process.exit(0)
    else
      changeScreen(-1)
  })
  screen.keymap('C-c', () => process.exit(0))
  screen.keymap('j', () => screen.down())
  screen.keymap('k', () => screen.up())
  screen.keymap('l', homeSelect)
  screen.keymap('s', () => {
    if (curThread != -1) {
      data.markAsRead(openThreads[curThread])
    }
  })
  screen.keymap('i', () => {
    if (curThread != -1) {
      setMode('INSERT')
      screen.prompt('', messageEdit)
    }
  })
  screen.keymap(':', () => {
    screen.prompt(':', (err, cmd) => {
      if (err) return console.error(err)
      if (!cmd) return
      doCmd(cmd.substr(1))
    })
  })
}

function setup(err) {
  if (err) return console.error(err)
  data.registerListener({ logLevel: 'error', selfListen: true, listenEvents: true }, events.onReceive)
  screen.init(getHeader, getStatus, getData)
  screen.unselect(true)
  setup_keys()
}

data.init({
  appStateFile: path.resolve(os.homedir(), './.mgsrcli_app_state'),
  refresh: screen.refresh
}, (cb) => {
  read({ prompt: 'login: ' }, (err, usr) => {
    if (err) return cb(err)
    read({ prompt: 'password: ', silent: true }, (err, pwd) => {
      if (err) return cb(err)
      cb(null, usr, pwd)
    })
  })
}, (cb) => {
  read({ prompt: 'TFA code: ' }, cb) 
}, setup)
// vim: fdm=marker sw=2
