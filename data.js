module.exports = (() => {
  const login = require('facebook-chat-api')
  const fs = require('fs')
  const os = require('os')
  const path = require('path')

  let data = {}

  let threadList = null
  let tidInfo = {}
  let uidInfo = {}
  let API = null

  let refreshFunc = null

  data.sendMessage = function(tid, msg, cb) {
    cb = cb || (x=>{})
    API.sendMessage({ body: msg }, tid, (err) => {
      if (err) return console.error(err)
      data.markAsRead(tid, cb)
    })
  }

  data.markAsRead = function(thread, cb) {  
    cb = cb || (x=>{})
    API.markAsRead(thread, (err) => {
      if (err) return cb(err)
      if (tidInfo[thread] && tidInfo[thread].history) {
        tidInfo[thread].history.forEach(x => {
          x.isUnread = false
        })
      }
      cb(null)
      refreshFunc()
    })
  }

  data.registerListener = function(opts, lsn) {
    API.setOptions(opts)
    API.listen(lsn)
  }

  data.getThreads = function(force) {
    if (force || !threadList) {
      API.getThreadList(0, 50, (err, list) => {
        if (err) return console.error(err)
        threadList = list.map(x => x.threadID)
        list.forEach(x => { tidInfo[x.threadID] = x })
        refreshFunc()
      })
      threadList = []
    }
    return threadList
  }

  data.getThreadHistory = function(tid, force) {
    tidInfo[tid] = tidInfo[tid] || {} 
    if (force || !tidInfo[tid].history) { 
      API.getThreadHistory(tid, 70, undefined, (err, history) => {
        if (err) return console.error(err)
        history.forEach(x => {
          if (x.timestamp <= tidInfo[tid].lastReadTimestamp || x.senderID == tid) {
            x.isReadOther = true
          }
        })
        tidInfo[tid].history = history
        refreshFunc()
      })
      tidInfo[tid].history = []
    }
    return tidInfo[tid].history
  }

  data.getNameInThread = function (uid, tid) {
    let tinfo = data.getThreadInfo(tid)
    let uinfo = data.getUserInfo(uid)
    if (tinfo.nicknames && tinfo.nicknames[uid])  
      return tinfo.nicknames[uid]
    else if (uinfo.name)  
      return uinfo.name
    else  
      return 'somebody'
  }

  function getInfo(id, force, fn, def, idInfo) {
    if (force || !idInfo[id]) {
      API[fn](id, (err, info) => {
        if (err) return console.error(err)
        if (info[id]) info = info[id]
        idInfo[id] = info
        refreshFunc()
      })
      idInfo[id] = def
    }
    return idInfo[id]
  }

  let loadingThread = id => ({
    threadID: id, participantIDs: [],
    name: '...', snippet: '...', unreadCount: 0,
    messageCount: 0, isCanonicalUser: true,
    isCanonical: true, canReply: true, threadType: 1
  })

  data.getThreadInfo = function(tid, force) {
    return getInfo(tid, force, 'getThreadInfo', loadingThread(tid), tidInfo)
  }

  let loadingUser = {
    name: '...', firstName: '...',
    type: 'user', isFriend: 'true',
    alternateName: '...'
  }

  data.getUserInfo = function(tid, force) {
    return getInfo(tid, force, 'getUserInfo', loadingUser, uidInfo)
  }

  function onLogin() {
    data.myuid = API.getCurrentUserID()
  }

  data.init = function(opts, askUserPwd, askTFA, callback) {
    callback = callback || (x=>{})
    if (API != null) 
      callback(new Error("data already initialized"))
    refreshFunc = opts.refresh
    let x = null
    try {
      x = opts.appStateFile && JSON.parse(fs.readFileSync(opts.appStateFile))
    } catch (ex) {
      if (ex.code != 'ENOENT') 
        return callback(x)
    }
    if (x) {
      opts.appState = x
      login(opts, (err, api) => {
        if (err) return callback(err)
        fs.writeFileSync(opts.appStateFile, JSON.stringify(api.getAppState()));
        API = api
        onLogin()
        callback(null)
      })
    } else {
      askUserPwd((err, user, pwd) => {
        if (err) return callback(err)
        opts.email = user
        opts.password = pwd
        login(opts, (loginerr, api) => {
          if (loginerr) {
            if (loginerr.error == 'login-approval') {
              askTFA((err, tfa) => {
                if (err) return callback(err)
                loginerr.continue(tfa)
              })
            } else {
              callback(loginerr)
            }
            return
          }
          if (opts.appStateFile)
            fs.writeFileSync(opts.appStateFile, JSON.stringify(api.getAppState()));
          API = api
          onLogin()
          callback(null)
        })
      })
    }
  }
  return data
})()
// vim: fdm=marker sw=2
