module.exports = (() => {
  const bls = require('blessed')
  const data = require('./data')
  let Format = {}

  Format.header = function(scr, threads) {
    if (scr == -1) {
      return 'q:Quit  l:Open'
    } else {
      return 'q:Back  i:Reply  s:Seen'
    }
  }

  let zadd = n => n < 10 ? '0' + n.toString() : n.toString()

  Format.status = function(scr, threads, mode) {
    let dt = new Date()
    x = ' {cyan-fg}[{/}' + zadd(dt.getHours()) + ':' + zadd(dt.getMinutes()) + '{cyan-fg}]{/} '
    if (mode) {
      x += '{bold}-- ' + mode + ' --{/bold} '
    }
    x += '{cyan-fg}[{/}' + (scr + 2) + ':' + (scr < 0 ? 'messenger' : data.getThreadInfo(threads[scr]).name) +
      '{cyan-fg}]{/} '
    threads.forEach(thr => {
      if (threads[scr] != thr && data.getThreadInfo(thr).unreadCount > 0)
        x += '{red-fg}*{/}' + (threads.indexOf(thr) + 2) + ' '
    })
    return x
  }

  Format.thread = function(tid) {
    let tdata = data.getThreadInfo(tid)
    //console.log(tdata)
    let name = tdata.name || ''
    let x = tdata.isCanonical ? '   ' : ' # '
    if (name == '' && tdata.isCanonical) {
      name = data.getUserInfo(tid).name
    } 
    if (tdata.unreadCount > 0) x += '{bold}'
    if (name == '') name = '<a nameless group>'
    x += '{red-fg}' + name + '{/red-fg} '
    x += '{#888888-fg}' + tdata.snippet + '{/#888888-fg} '
    if (tdata.unreadCount > 0) x += '{/bold}'
    x += '{|}{black-fg}' + tid
    return x
  }

  Format.user = function (uid, msg) {
    let name = data.getNameInThread(uid, msg.threadID)
    let x = '<'
    x += uid == data.myuid ? '{bold}' : '{cyan-fg}'
    x += name
    x += '{/}'
    x += '>'
    return x
  }

  Format.body = function (body, msg) {
    return bls.escape(body).replace(/\b[_*]+.*?[_*]+\b/g, '{bold}$&{/bold}')
  }

  Format.message = function (msg) {
    let dt = new Date(+msg.timestamp)
    let x = ' ' + zadd(dt.getHours()) + ':' + zadd(dt.getMinutes()) + ' '
    x += (!msg.isReadOther ? '{green-fg}*{/}' : ' ')
    x += (msg.isUnread == undefined || msg.isUnread) ? '{red-fg}*{/} ' : '  '
    x += Format.user(msg.senderID, msg) + ' '
    x += Format.body(msg.body, msg) + ' '
    x += '{|}{black-fg}' + msg.messageID
    return x
  }

  Format.event = function (ev) {
    let dt = new Date(+ev.timestamp)
    let x = ' ' + zadd(dt.getHours()) + ':' + zadd(dt.getMinutes()) + ' '
    x += '{bold}{blue-fg}-{/blue-fg}!{blue-fg}-{/} '
    x += ev.logMessageBody || ev.snippet
    x += '{black-fg}' + ev.messageID
    return x
  }

  Format.chatLine = function (line) {
    switch (line.type) {
      case 'message': return Format.message(line)
      case 'event': return Format.event(line)
      default: console.error(new Error('unknown line type ' + line.type))
    }
  }

  return Format
})()
// vim: fdm=marker sw=2

