module.exports = (() => {
  const data = require('./data.js')
  const screen = require('./screen.js')
  let events = {}

  events.onMessageOrEvent = function(msg) {
    if (msg.threadID == msg.senderID) {
      msg.isReadOther = true
    } else if (data.myuid == msg.senderID) {
      msg.isUnread = false
    }
    let threads = data.getThreads()
    let ind = threads.indexOf(msg.threadID)
    if (ind != -1) {
      threads.splice(ind, 1)
    }
    threads.unshift(msg.threadID)
    data.getThreadInfo(msg.threadID).snippet = msg.body || msg.logMessageBody || msg.snippet || '...'
    data.getThreadHistory(msg.threadID).push(msg)
    screen.refresh()
  }

  events.onMessage = function(msg) {
    events.onMessageOrEvent(msg)
  }

  events.onEvent = function(msg) {
    events.onMessageOrEvent(msg)
  }

  events.onTyp = function(msg) { }
  events.onRead = function(msg) { 
    data.getThreadHistory(msg.threadID).forEach(x => {
      if (x.timestamp <= msg.time || x.senderID == data.myuid) {
        x.isUnread = false
      }
    })
    screen.refresh()
  }
  events.onReadReciept = function (msg) {
    if (msg.reader == msg.threadID) {
      data.getThreadHistory(msg.threadID).forEach(x => {
        if (x.timestamp <= msg.time || x.senderID == msg.reader) {
          x.isReadOther = true
        }
      })
      screen.refresh()
    }
  }
  events.onMessageReaction = function (msg) { }
  events.onPresence = function (msg) { }

  events.onReceive = function(err, ev) {
    if (err) return console.error(err)
    switch(ev.type) {
      case 'message': return events.onMessage(ev)
      case 'event': return events.onEvent(ev)
      case 'typ': return events.onTyp(ev)
      case 'read': return events.onRead(ev)
      case 'read_receipt': return events.onReadReciept(ev)
      case 'message_reaction': return events.onMessageReaction(ev)
      case 'presence': return events.onPresence(ev)
      default: return console.error(new Error('unknown message type ' + ev.type))
    }
  }

  return events
})()

// vim: fdm=marker sw=2
